#!/usr/bin/env bash

echo "Starting Elasticsearch..."

su hw3 -c "./elasticsearch-6.5.1/bin/elasticsearch -d"

while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' localhost:9200)" != "200" ]];
do sleep 2;
done

echo "Done"


echo "Starting Logstash..."

su hw3 -c "./logstash-6.5.1/bin/logstash -f /home/hw3/logstash.conf >/dev/null &"

echo "Starting Grafana..."

cd /usr/share/grafana
/usr/sbin/grafana-server 1>/dev/null &
cd /home/hw3

while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' http://admin:admin@localhost:3000/api/datasources)" != "200" ]];
do sleep 2;
done

echo "Add datasource to Grafana"

curl -s -o /dev/null -X POST 'http://admin:admin@localhost:3000/api/datasources' -H "Content-Type: application/json"  -d '{"name":"twitter","type":"elasticsearch","url":"http://localhost:9200","access":"proxy","database":"twitter","jsonData":{"esVersion":5,"timeField":"@timestamp"}}'

echo "Done"

java -jar ./twitterSrc/target/homework3-1.0-SNAPSHOT-jar-with-dependencies.jar 10
