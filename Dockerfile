FROM ubuntu:18.04

EXPOSE 3000
# Install requires packages
RUN apt-get update
RUN apt-get install -y openjdk-8-jdk maven wget curl tar

# Create user
WORKDIR /home/hw3
RUN useradd hw3
RUN chown hw3 .

# Download and untar elasticsearch
RUN wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.5.1.tar.gz
RUN tar -xzf elasticsearch-6.5.1.tar.gz
RUN rm elasticsearch-6.5.1.tar.gz
RUN chown hw3 -R elasticsearch-6.5.1

# Dowload and untar logstash
RUN wget https://artifacts.elastic.co/downloads/logstash/logstash-6.5.1.tar.gz
RUN tar -xzf logstash-6.5.1.tar.gz
RUN rm logstash-6.5.1.tar.gz
RUN chown hw3 -R logstash-6.5.1

# Download && install Grafana
RUN wget https://s3-us-west-2.amazonaws.com/grafana-releases/release/grafana_5.1.4_amd64.deb
RUN apt-get install -y adduser libfontconfig
RUN dpkg -i grafana_5.1.4_amd64.deb
RUN rm grafana_5.1.4_amd64.deb

# Build project
RUN mkdir twitterSrc
COPY src /home/hw3/twitterSrc/src
COPY pom.xml /home/hw3/twitterSrc/
RUN cd twitterSrc && mvn package
RUN chown hw3 -R twitterSrc

# Copy logstash config
COPY scripts/logstash.conf /home/hw3/logstash.conf

# Copy main script
COPY scripts/start.sh /home/hw3/start.sh

# Ugly hack: ubuntu 18.04 goes with java 10 default
# 2 - java8 selection number
RUN echo 2 | update-alternatives --config java >/dev/null
# Run command
RUN chmod 777 /home/hw3/start.sh
CMD ["/home/hw3/start.sh"]
