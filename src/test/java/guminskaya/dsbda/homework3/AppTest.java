package guminskaya.dsbda.homework3;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import twitter4j.StatusDeletionNotice;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

/**
 * @author aguminskaya
 * @since 06/12/2018
 */
@RunWith(MockitoJUnitRunner.class)
public class AppTest {

    @Test
    public void testOneCountOneInterval() throws InterruptedException {

        final TestAppender appender = new TestAppender();
        final Logger logger = Logger.getRootLogger();
        logger.addAppender(appender);
        DeleteEventListener deleteEventListener = new DeleteEventListener();
        deleteEventListener.setInterval(1);
        deleteEventListener.onDeletionNotice(mock(StatusDeletionNotice.class));
        Thread.sleep(1000);
        deleteEventListener.onDeletionNotice(mock(StatusDeletionNotice.class));
        final List<LoggingEvent> log = appender.getLog();
        final LoggingEvent firstLogEntry = log.get(0);
        assertEquals(firstLogEntry.getLevel(), Level.INFO);
        assertEquals(firstLogEntry.getMessage(), 1);
        assertEquals(firstLogEntry.getLoggerName(), DeleteEventListener.class.getCanonicalName());
    }

    @Test
    public void testTwoCountOneInterval() throws InterruptedException {
        final TestAppender appender = new TestAppender();
        final Logger logger = Logger.getRootLogger();
        logger.addAppender(appender);
        DeleteEventListener deleteEventListener = new DeleteEventListener();
        deleteEventListener.setInterval(1);
        deleteEventListener.onDeletionNotice(mock(StatusDeletionNotice.class));
        deleteEventListener.onDeletionNotice(mock(StatusDeletionNotice.class));
        Thread.sleep(1000);
        deleteEventListener.onDeletionNotice(mock(StatusDeletionNotice.class));
        final List<LoggingEvent> log = appender.getLog();
        final LoggingEvent firstLogEntry = log.get(0);
        assertEquals(firstLogEntry.getLevel(), Level.INFO);
        assertEquals(firstLogEntry.getMessage(), 2);
        assertEquals(firstLogEntry.getLoggerName(), DeleteEventListener.class.getCanonicalName());
    }

    @Test
    public void testTwoIntervals() throws InterruptedException {
        final TestAppender appender = new TestAppender();
        final Logger logger = Logger.getRootLogger();
        logger.addAppender(appender);
        DeleteEventListener deleteEventListener = new DeleteEventListener();

        deleteEventListener.setInterval(2);

        deleteEventListener.onDeletionNotice(mock(StatusDeletionNotice.class));
        deleteEventListener.onDeletionNotice(mock(StatusDeletionNotice.class));
        Thread.sleep(2000);

        deleteEventListener.onDeletionNotice(mock(StatusDeletionNotice.class));
        deleteEventListener.onDeletionNotice(mock(StatusDeletionNotice.class));
        deleteEventListener.onDeletionNotice(mock(StatusDeletionNotice.class));
        Thread.sleep(2000);
        deleteEventListener.onDeletionNotice(mock(StatusDeletionNotice.class));

        final List<LoggingEvent> log = appender.getLog();
        final LoggingEvent firstLogEntry = log.get(0);
        assertEquals(firstLogEntry.getLevel(), Level.INFO);
        assertEquals(firstLogEntry.getMessage(), 2);
        assertEquals(firstLogEntry.getLoggerName(), DeleteEventListener.class.getCanonicalName());

        final LoggingEvent secondLogEntry = log.get(1);
        assertEquals(secondLogEntry.getLevel(), Level.INFO);
        assertEquals(secondLogEntry.getMessage(), 3);
        assertEquals(secondLogEntry.getLoggerName(), DeleteEventListener.class.getCanonicalName());
    }

    @Test
    public void testNoLogCount() {
        final TestAppender appender = new TestAppender();
        final Logger logger = Logger.getRootLogger();
        logger.addAppender(appender);
        DeleteEventListener deleteEventListener = new DeleteEventListener();

        deleteEventListener.setInterval(2);

        deleteEventListener.onDeletionNotice(mock(StatusDeletionNotice.class));
        deleteEventListener.onDeletionNotice(mock(StatusDeletionNotice.class));
        final List<LoggingEvent> log = appender.getLog();
        assertEquals(log.size(), 0);
    }
}

class TestAppender extends AppenderSkeleton {
    private final List<LoggingEvent> log = new ArrayList<>();

    @Override
    public boolean requiresLayout() {
        return false;
    }

    @Override
    protected void append(final LoggingEvent loggingEvent) {
        log.add(loggingEvent);
    }

    @Override
    public void close() {
    }

    public List<LoggingEvent> getLog() {
        return new ArrayList<>(log);
    }
}