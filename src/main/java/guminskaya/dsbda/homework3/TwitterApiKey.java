package guminskaya.dsbda.homework3;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Модель ключей пользователя TwitterAPI
 * @author aguminskaya
 * @since 06/12/2018
 */
@Data
@AllArgsConstructor
public class TwitterApiKey {
    private String consumerKey;
    private String consumerSecret;
    private String accessToken;
    private String accessTokenSecret;

}
