package guminskaya.dsbda.homework3;

import lombok.Setter;
import lombok.extern.log4j.Log4j;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Имплементирует {@link StatusListener}. На событие удаления производит подсчет событий и сохраняет запись
 * в лог.
 * @author aguminskaya
 * @since 06/12/2018
 */
@Log4j
public class DeleteEventListener implements StatusListener {
    private int count = 0;
    @Setter
    private int interval = 60;

    private LocalDateTime previousTime;

    @Override
    public void onStatus(Status status) {

    }

    @Override
    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
        // только стартанули
        if (previousTime == null) {
            previousTime = LocalDateTime.now();
        }
        LocalDateTime nowTime = LocalDateTime.now();
        if (ChronoUnit.SECONDS.between(previousTime, nowTime) >= interval) {
            log.info(count);
            count = 1;
            previousTime = nowTime;
        } else {
            ++count;
        }
    }

    @Override
    public void onTrackLimitationNotice(int i) {

    }

    @Override
    public void onScrubGeo(long l, long l1) {

    }

    @Override
    public void onStallWarning(StallWarning stallWarning) {

    }

    @Override
    public void onException(Exception e) {

    }
}
