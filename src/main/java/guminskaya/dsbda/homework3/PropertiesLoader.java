package guminskaya.dsbda.homework3;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.IOException;
import java.util.Properties;

/**
 * Вспомогательный класс для загрузки конфигурации, а именно ключей пользователя для пользования TwitterAPI.
 * хххMake Twitter great again! No russian hackers!xxx
 * @author aguminskaya
 * @since 06/12/2018
 */
@Data
@AllArgsConstructor
public class PropertiesLoader {

    private String propertyFile;

    public TwitterApiKey loadTwitterApiKey() {
        Properties property = new Properties();

        try {
            property.load(getClass().getClassLoader().getResourceAsStream(propertyFile));
            String consumerKey = property.getProperty("consumerKey");
            String consumerSecret = property.getProperty("consumerSecret");
            String accessToken = property.getProperty("accessToken");
            String accessTokenSecret = property.getProperty("accessTokenSecret");
            return new TwitterApiKey(consumerKey, consumerSecret, accessToken, accessTokenSecret);
        } catch (IOException e) {
            throw new RuntimeException("application.properties must exist!");
        }
    }
}
