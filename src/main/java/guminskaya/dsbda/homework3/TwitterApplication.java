package guminskaya.dsbda.homework3;

import lombok.extern.log4j.Log4j;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * @author Anna Guminskaya
 * @since 01/12/2018.
 */
@Log4j
public class TwitterApplication {

    /**
     * Входная точка программы.
     * @param args - входной параметр - интервал, за который суммируем события в секундах.
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            log.info("USAGE: java -jar homework3-1.0-SNAPSHOT-jar-with-dependencies.jar [intervalInSeconds]");
            log.info("default interval is 60 seconds");
        }
        PropertiesLoader propertiesLoader = new PropertiesLoader("application.properties");
        TwitterApiKey twitterApiKey = propertiesLoader.loadTwitterApiKey();

        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(twitterApiKey.getConsumerKey())
                .setOAuthConsumerSecret(twitterApiKey.getConsumerSecret())
                .setOAuthAccessToken(twitterApiKey.getAccessToken())
                .setOAuthAccessTokenSecret(twitterApiKey.getAccessTokenSecret());

        TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
        DeleteEventListener deleteEventListener = new DeleteEventListener();
        try {
            if (args.length != 0) {
                deleteEventListener.setInterval(Integer.valueOf(args[0]));
            }
        } catch (NumberFormatException e) {
            log.warn("USAGE: java -jar homework3-1.0-SNAPSHOT-jar-with-dependencies.jar [intervalInSeconds]");
            log.warn("Argument must be digit! Set interval to 60 seconds");
        }
        twitterStream.addListener(deleteEventListener);
        twitterStream.sample();

        //shutdown application clearly
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            twitterStream.cleanUp();
            twitterStream.shutdown();
            log.info("Application shutdown");
        }));
    }

}
