# Отчет  

## Сборка docker image
``docker build -t twitter_elg . ``

## Старт контейнера
`` docker run -i -p 3000:3000 twitter_elg``

### График количества удаленных сообщений по времени

![Graph](screenshots/Graph.png)

### Скриншот выполненных тестов 

![Graph](screenshots/Tests.png)

### Диаграмма взаимодействия

![UML](screenshots/diagram.png)




